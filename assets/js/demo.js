$().ready(function() {
    $sidebar = $('.sidebar');
    $sidebar_img_container = $sidebar.find('.sidebar-background');

    $full_page = $('.full-page');

    $sidebar_responsive = $('body > .navbar-collapse');

    window_width = $(window).width();

    fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

    if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
        if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('show');
        }

    }

    $('.fixed-plugin a').click(function(event) {
        if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
                event.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
        }
    });

    $('.fixed-plugin .background-color span').click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');

        var new_color = $(this).data('color');

        if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
        }

        if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
        }

        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
        }
    });

    $('.fixed-plugin .img-holder').click(function() {
        $full_page_background = $('.full-page-background');

        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');


        var new_image = $(this).find("img").attr('src');

        if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
                $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
                $sidebar_img_container.fadeIn('fast');
            });
        }

        if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
                $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
                $full_page_background.fadeIn('fast');
            });
        }

        if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
        }

        if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
        }
    });

    $('.switch input').on("switchChange.bootstrapSwitch", function() {

        $full_page_background = $('.full-page-background');

        $input = $(this);

        if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
                $sidebar_img_container.fadeIn('fast');
                $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
                $full_page_background.fadeIn('fast');
                $full_page.attr('data-image', '#');
            }

            background_image = true;
        } else {
            if ($sidebar_img_container.length != 0) {
                $sidebar.removeAttr('data-image');
                $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
                $full_page.removeAttr('data-image', '#');
                $full_page_background.fadeOut('fast');
            }

            background_image = false;
        }
    });
});

type = ['primary', 'info', 'success', 'warning', 'danger'];

demo = {

    MQTTConnect: function(callback){

        function SigV4Utils(){}

        SigV4Utils.sign = function(key, msg) {
            var hash = CryptoJS.HmacSHA256(msg, key);
            return hash.toString(CryptoJS.enc.Hex);
        };

        SigV4Utils.sha256 = function(msg) {
            var hash = CryptoJS.SHA256(msg);
            return hash.toString(CryptoJS.enc.Hex);
        };

        SigV4Utils.getSignatureKey = function(key, dateStamp, regionName, serviceName) {
            var kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key);
            var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
            var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
            var kSigning = CryptoJS.HmacSHA256('aws4_request', kService);
            return kSigning;
        };

        function createEndpoint(regionName, awsIotEndpoint, accessKey, secretKey) {
            var time = moment.utc();
            var dateStamp = time.format('YYYYMMDD');
            var amzdate = dateStamp + 'T' + time.format('HHmmss') + 'Z';
            var service = 'iotdevicegateway';
            var region = regionName;
            var secretKey = secretKey;
            var accessKey = accessKey;
            var algorithm = 'AWS4-HMAC-SHA256';
            var method = 'GET';
            var canonicalUri = '/mqtt';
            var host = awsIotEndpoint;
            var credentialScope = dateStamp + '/' + region + '/' + service + '/' + 'aws4_request';
            var canonicalQuerystring = 'X-Amz-Algorithm=AWS4-HMAC-SHA256';
            canonicalQuerystring += '&X-Amz-Credential=' + encodeURIComponent(accessKey + '/' + credentialScope);
            canonicalQuerystring += '&X-Amz-Date=' + amzdate;
            canonicalQuerystring += '&X-Amz-SignedHeaders=host';
            var canonicalHeaders = 'host:' + host + '\n';
            var payloadHash = SigV4Utils.sha256('');
            var canonicalRequest = method + '\n' + canonicalUri + '\n' + canonicalQuerystring + '\n' + canonicalHeaders + '\nhost\n' + payloadHash;
            var stringToSign = algorithm + '\n' +  amzdate + '\n' +  credentialScope + '\n' +  SigV4Utils.sha256(canonicalRequest);
            var signingKey = SigV4Utils.getSignatureKey(secretKey, dateStamp, region, service);
            var signature = SigV4Utils.sign(signingKey, stringToSign);
            canonicalQuerystring += '&X-Amz-Signature=' + signature;
           return 'wss://' + host + canonicalUri + '?' + canonicalQuerystring;
        }
        var endpoint = createEndpoint(
            'us-east-2',  // YOUR REGION
            'a1rv95j8gy7wcr-ats.iot.us-east-2.amazonaws.com', // YOUR IoT ENDPOINT WITH ATS TO BE ACCEPTED BY CHROME
            'AKIAJXYFPJB4ZCGXLXAA', // YOUR ACCESS KEY
            'ZwzyV8dJwTBxhsMPqayKXnU78DCBEFstVKk6D4ec'); //YOUR SECRET ACCESS KEY

        var clientId = 'TE-GATEWAY01-DASHBOARD-JS-client01';
        var client = new Paho.MQTT.Client(endpoint, clientId);
        var connectOptions = {
            useSSL: true,
            timeout: 3,
            mqttVersion: 4,
            onSuccess: subscribe
        };
        client.connect(connectOptions);
        client.onMessageArrived = onMessage;
        client.onConnectionLost = function(e) {
            console.log(e) 
        };

        function subscribe() {
           client.subscribe("TE/SAS/access");
           console.log("subscribing to topic SAS access");
        }

        function onMessage(message) {
            var msg = JSON.parse(message.payloadString.replace('\0',''));
            if (message.destinationName == "TE/SAS/access"){
                if (msg.access_granted == true){
                    card_holder = msg.card_holder
                    demo.showNotification(card_holder + " entered the room!"); 
                }else if(msg.access_granted == false){
                    demo.showNotification("Someone tried to enter the room - access denied!");  
                }        
            }
            console.log(msg);
        } 

        return client

    },


    showNotification: function(message, from, align) {
        color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: "nc-icon nc-app",
            message: message

        }, {
            type: type[color],
            timer: 8000,
            placement: {
                from: from,
                align: align
            }
        });
    }

}

